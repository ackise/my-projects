# step-project-2nd

DAN-IT 
Step Project Forkio
[Forkio live version](https://vchmichael.gitlab.io/step-project-2/index.html)

***Studens***
-----------------------------------

- Michael Velychkevych (dev1)
- Maxim Ivanov (dev2)

***Technologies***
-----------------------------------
- Gulp
- HTML5/SCSS/JS
- Gitlab Pages

***Dev1***
-----------------------------------
Header, Dropdown menu, Section "People Are Talking About Fork", Git, GitLab Pages

***Dev2***
-----------------------------------
Section "Revolutionary Editor", "Here is what you get", "Fork Subscription Pricing", buttons: watch/star/fork.

***Team***
-----------------------------------

Readme file, gulpfile, code review, merge branches into master
