import {Form} from './form'
export class Modal {
    constructor(){
        this.container = document.createElement('div')
        this.closeBtn = document.createElement('button')
        this.back = document.createElement('div')
    }
    init() {
        this.render();
        this.registForm()
       
    }
    initVisit(){
        this.render();
        this.addVisit();
    }
    render(){
        this.container.classList.add('modal-login')
        document.body.append(this.container)
        this.closeBtn.classList.add('modal-login__closebtn')
        this.closeBtn.innerText = 'X'
        this.back.classList.add('modal-login__back')
        this.back.append(this.closeBtn)
        this.container.append(this.back)

        this.closeBtn.addEventListener('click',onRemove.bind(this))
        function onRemove(){
            this.container.remove()
            localStorage.setItem('modal',true)
            localStorage.setItem('visit',true)
            this.closeBtn.removeEventListener('click',onRemove)
            
        }
       
    }

    registForm(){
        let newForm = new Form(this.container).renderLogin()
    }
    addVisit(){
        let newForm = new Form(this.container).renderVisit();
    }
    editCard(visit) {
        this.render()
        let newForm = new Form(this.container).renederEdit(visit);
       
    }
}