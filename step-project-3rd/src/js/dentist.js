import {Visit} from './visit'
import { Card } from './card';


export class VisitDentist extends Visit {
    constructor(){
        super()
    }

    visitDentist(){
        super.renderForm();
        const dentistFields = document.createElement('div');
        dentistFields.classList.add('modal-login__doctor-fields');
        

        const lastVisit = document.createElement('input')
        lastVisit.setAttribute('placeholder', 'Date of last visit')
        lastVisit.classList.add('modal-login__login-input')
        lastVisit.setAttribute('onfocus',`this.type='date'`)
        
        dentistFields.append(lastVisit)
        document.querySelector('.modal-login__createvisitbtn').before(dentistFields)

        this.createVisitBtn.addEventListener('click', () => {
           
            let keyValues = {
                title: this.visitTitle.value,
                description: this.visitDescription.value,
                doctor: document.querySelector('.select-doctor').value,
                lastvisit: lastVisit.value,
                
                
                urgency: this.urgentList.value,
                fullname: this.visitFullName.value,
                visitreason: this.visitReason.value,
                status: 'open'
            }
            const formFields = Object.values(keyValues)
                const newArr = formFields.filter(e => e !== "");
                if (formFields.length === newArr.length) {

                    fetch('https://cards.danit.com.ua/cards', {
                        method: 'POST',
                        body: JSON.stringify(keyValues),
                        headers: {
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }
                    })
                        .then(res => res.json())
                        .then(res => new Card(res).render())
                        document.querySelector('.modal-login__closebtn').click()
                } else {
                    if(!document.querySelector('.alert')){
                        const alert = document.createElement('span')
                    alert.innerHTML = 'Please fill all fields'
                    alert.classList.add('alert')
                    document.querySelector('.modal-login__createvisitbtn').before(alert)
                    }
                    

                }
            

            })
       
    }
}