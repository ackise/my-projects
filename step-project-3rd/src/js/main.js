import {App} from './app'
import '../scss/app.scss'
import '../scss/modal.scss'
import '../scss/board.scss'
import ProjectLogo from '../assets/img/logo.png'


const newApp = new App(ProjectLogo)
newApp.init()