export class Visit {
    constructor() {
        this.visitReason = document.createElement('input');
        this.visitDescription = document.createElement('textarea');
        this.visitFullName = document.createElement('input')
        this.visitTitle = document.createElement('input')
        this.urgentList = document.createElement('select')
        this.createVisitBtn = document.createElement('button')
    }
    renderForm() {
        const visitFields = document.createElement('div');
        visitFields.classList.add('modal-login__visit-fields');
        this.visitReason.setAttribute('placeholder', 'Visit reason')
        this.visitFullName.setAttribute('placeholder', 'Full Name')
        this.visitDescription.setAttribute('placeholder', 'Visit description')
        this.visitTitle.setAttribute('placeholder', 'Visit Title')
        this.createVisitBtn.classList.add('modal-login__createvisitbtn')
        this.createVisitBtn.innerText = 'Create visit'
        
        const fields =[this.visitTitle, this.visitFullName, this.urgentList, this.visitReason,this.visitDescription,this.createVisitBtn ]
        fields.forEach(e => e.classList.add('modal-login__login-input'))
        

        const urgentItems = ['Choose urgency','Common','Prioritized','Urgent']
        this.urgentList.classList.add('modal-login__urgentlist')
        urgentItems.forEach(element =>{
            const option = document.createElement('option')
            option.setAttribute('required',true)
            option.innerText = element
            this.urgentList.append(option)
        })
        
        this.urgentList.children[0].setAttribute('disabled', true)
        this.urgentList.children[0].setAttribute('value','Common')
        visitFields.append(...fields);
        document.querySelector('.select-doctor').after(visitFields)

        
    }
}