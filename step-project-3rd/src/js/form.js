import {Modal} from './modal'
import {Board} from './board'
import {VisitCardiologist} from './cardiologist'
import {VisitDentist} from './dentist'
import {VisitTherapist} from './therapepht'

export class Form {
    constructor(container) {
        this.form = document.createElement('form')
        this.container = container
        this.submitBtn = document.createElement('button')


    }

    renderLogin() {
        const logText = document.createElement('span')
        logText.classList.add('modal-login__logtext')
        logText.innerHTML = "Enter your login"

        const logInput = document.createElement('input')
        logInput.classList.add('modal-login__login-input')

        const passText = document.createElement('span')
        passText.classList.add('modal-login__password-text')
        passText.innerHTML = "Enter your password"

        const passInput = document.createElement('input')
        passInput.classList.add('modal-login__password-input')
        passInput.setAttribute('type', 'password')


        this.submitBtn.classList.add('modal-login__submit-btn')
        this.submitBtn.innerText = 'Log in'

        this.submitBtn.addEventListener('click', () => {
            fetch('https://cards.danit.com.ua/login', {
                method: 'POST',
                body: JSON.stringify({
                    email: logInput.value,
                    password: passInput.value,
                }),
            })
                .then((res) => {
                    if (res.status >= 200 && res.status < 300) {
                        return res;
                    } else {
                        let error = new Error(res.statusText);
                        error.response = res;
                        throw error
                    }
                })
                .then(res => res.json())
                .then(data => {
                    if (data.status === 'Error') {
                        alert(data.message)
                    } else {
                        localStorage.setItem('token', data.token)
                        this.container.remove()
                        this.successReg()
                    }
                })

        })

        this.container.append(logText, logInput, passText, passInput, this.submitBtn)

    }

    successReg() {
        this.btnAddVisit = document.createElement('button');
        this.btnAddVisit.classList.add('btn')
        this.btnAddVisit.innerText = 'Add visit'
        document.querySelector('.btn').remove()
        document.querySelector('.header').append(this.btnAddVisit)
        let board = new Board().init()
        this.btnAddVisit.addEventListener('click', () => {

            if (localStorage.getItem('visit') === 'true') {
                const newModal = new Modal().initVisit()
                localStorage.setItem('visit', false)
            }

        })
    }

    renderVisit() {
        const list = document.createElement('select')
        list.classList.add('select-doctor')

        const docsList = ['Select your doctor', 'Сardiologist', 'Dantist', 'Therapist']

        docsList.forEach(element => {
            const option = document.createElement('option')
            list.append(option)
            option.setAttribute('required', true)
            option.innerText = element
        })


        this.container.append(list)
        list.children[0].setAttribute('disabled', true)


        list.onchange = function () {
            if (this.value === 'Сardiologist') {
                if (document.querySelector('.modal-login__doctor-fields')) {
                    document.querySelectorAll('.modal-login__doctor-fields').forEach(e => e.remove())
                    document.querySelectorAll('.modal-login__visit-fields').forEach(e => e.remove())
                }
                const docVisitCardio = new VisitCardiologist().visitCardiologist();

            }
            if (this.value === 'Dantist') {
                if (document.querySelector('.modal-login__doctor-fields')) {
                    document.querySelectorAll('.modal-login__doctor-fields').forEach(e => e.remove())
                    document.querySelectorAll('.modal-login__visit-fields').forEach(e => e.remove())

                }
                const docVisitDentist = new VisitDentist().visitDentist();

            }
            if (this.value === 'Therapist') {
                if (document.querySelector('.modal-login__doctor-fields')) {
                    document.querySelectorAll('.modal-login__doctor-fields').forEach(e => e.remove())
                    document.querySelectorAll('.modal-login__visit-fields').forEach(e => e.remove())

                }
                const docVisitTherapist = new VisitTherapist().visitTherapist();
            }

        }
    }

    renederEdit(visit) {
        
        const status = document.createElement('select')
        const statuses = ['open', 'done']
        const age = document.createElement('input');
        const blood = document.createElement('input');
        const diseases = document.createElement('input');
        const bodyIndex = document.createElement('input');
        const date = document.createElement('input');
        statuses.forEach(e => {
            const option = document.createElement('option')
            option.setAttribute('required', true)
            option.innerText = e
            status.append(option)
        })
        const visitReason = document.createElement('input');
        const visitDescription = document.createElement('textarea');
        const visitFullName = document.createElement('input')
        const visitTitle = document.createElement('input')
        const urgentItems = ['Choose urgency', 'Common', 'Prioritized', 'Urgent']
        const urgentList = document.createElement('select')
        urgentList.classList.add('modal-login__urgentlist')
        urgentItems.forEach(element => {
            const option = document.createElement('option')
            option.setAttribute('required', true)
            option.innerText = element
            urgentList.append(option)
        })
        urgentList.children[0].setAttribute('disabled', true)
        urgentList.children[0].setAttribute('value','Common')
        visitReason.value = visit.visitreason
        visitDescription.value = visit.description
        visitFullName.value = visit.fullname
        visitTitle.value = visit.title

        const visitFields = document.createElement('div')
        visitFields.classList.add('modal-login__visit-fields');
        if (visit.doctor === 'Therapist') {
            const age = document.createElement('input');
            age.classList.add('modal-login__login-input')
            age.value = visit.age
            visitFields.append(age)
        }
        if (visit.doctor === 'Dantist') {
            date.classList.add('modal-login__login-input')
            date.setAttribute('onfocus',`this.type='date'`)
            date.value = visit.lastvisit
            visitFields.append(date)
        }
        if (visit.doctor === 'Сardiologist') {
            age.classList.add('modal-login__login-input')
            blood.classList.add('modal-login__login-input')
            bodyIndex.classList.add('modal-login__login-input')
            diseases.classList.add('modal-login__login-input')
            age.value = visit.age
            blood.value = visit.bloodPresure
            bodyIndex.value = visit.bodyIndex
            diseases.value = visit.diseases
            visitFields.append(age, blood, bodyIndex, diseases)
        }
        const jktym = [visitReason, visitTitle, visitDescription, visitFullName,urgentList, status]
        jktym.forEach(e => e.classList.add('modal-login__login-input'))

        visitFields.append(...jktym)
        const saveEdit = document.createElement('button')
        saveEdit.classList.add('modal-login__createvisitbtn')
        saveEdit.innerText = 'Save'

        this.container.append(visitFields, saveEdit)
        saveEdit.addEventListener('click', () => {
            const bodyCardio = {
                title: visitTitle.value,
                description: visitDescription.value,
                doctor: visit.doctor,
                age: age.value,
                bloodPresure: blood.value,
                bodyIndex: bodyIndex.value,
                diseases: diseases.value,
                urgency: urgentList.value,
                fullname: visitFullName.value,
                visitreason: visitReason.value,
                status: status.value
            }
            const bodyDantist = {
                title: visitTitle.value,
                description: visitDescription.value,
                doctor: visit.doctor,
                lastvisit: date.value,
                urgency: urgentList.value,
                fullname: visitFullName.value,
                visitreason: visitReason.value,
                status: status.value
            }
            const bodyTherapist = {
                title: visitTitle.value,
                description: visitDescription.value,
                doctor: visit.doctor,
                age: age.value,
                urgency: urgentList.value,
                fullname: visitFullName.value,
                visitreason: visitReason.value,
                status: status.value
                
            }
            if (visit.doctor === 'Сardiologist') {
                fetch(`https://cards.danit.com.ua/cards/${visit.id}`, {
                    method: 'PUT',
                    body: JSON.stringify(bodyCardio),
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    }
                })
                    .then((res) => {
                        if (res.status >= 200 && res.status < 300) {
                            return res;
                        } else {
                            let error = new Error(res.statusText);
                            error.response = res;
                            throw error
                        }
                    })
                    .then(res => res.json())
                    .then(data => {
                        if (data.status === 'Error') {
                            alert(data.message)
                        } else {
                            window.location.reload()
                        }
                    })

            } 
            if(visit.doctor === 'Dantist'){
                fetch(`https://cards.danit.com.ua/cards/${visit.id}`, {
                    method: 'PUT',
                    body: JSON.stringify(bodyDantist),
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    }
                })
                    .then((res) => {
                        if (res.status >= 200 && res.status < 300) {
                            return res;
                        } else {
                            let error = new Error(res.statusText);
                            error.response = res;
                            throw error
                        }
                    })
                    .then(res => res.json())
                    .then(data => {
                        if (data.status === 'Error') {
                            alert(data.message)
                        } else {
                            window.location.reload()
                        }
                    })
            }
            if(visit.doctor === 'Therapist'){
                fetch(`https://cards.danit.com.ua/cards/${visit.id}`, {
                    method: 'PUT',
                    body: JSON.stringify(bodyTherapist),
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    }
                })
                    .then((res) => {
                        if (res.status >= 200 && res.status < 300) {
                            return res;
                        } else {
                            let error = new Error(res.statusText);
                            error.response = res;
                            throw error
                        }
                    })
                    .then(res => res.json())
                    .then(data => {
                        if (data.status === 'Error') {
                            alert(data.message)
                        } else {
                            window.location.reload()
                        }
                    })

            }


        })

    }
}

