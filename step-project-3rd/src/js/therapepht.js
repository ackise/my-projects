import {Visit} from './visit'
import {Card} from './card';


export class VisitTherapist extends Visit {
    constructor() {
        super()
    }

    visitTherapist() {
        super.renderForm();
        const therapistFields = document.createElement('div');
        therapistFields.classList.add('modal-login__doctor-fields');

        const age = document.createElement('input')
        age.setAttribute('placeholder', 'Your age')
        age.classList.add('modal-login__login-input')
        age.setAttribute('type', 'number')

        therapistFields.append(age)
        document.querySelector('.modal-login__createvisitbtn').before(therapistFields)

        this.createVisitBtn.addEventListener('click', () => {

            let keyValues = {
                title: this.visitTitle.value,
                description: this.visitDescription.value,
                doctor: document.querySelector('.select-doctor').value,
                age: age.value,
                urgency: this.urgentList.value,
                fullname: this.visitFullName.value,
                visitreason: this.visitReason.value,
                status: 'open'
            }
            const formFields = Object.values(keyValues)
            const newArr = formFields.filter(e => e !== "");
            if (formFields.length === newArr.length) {

                fetch('https://cards.danit.com.ua/cards', {
                    method: 'POST',
                    body: JSON.stringify(keyValues),
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    }
                })
                    .then(res => res.json())
                    .then(res => new Card(res).render())
                document.querySelector('.modal-login__closebtn').click()
            } else {
                if (!document.querySelector('.alert')) {
                    const alert = document.createElement('span')
                    alert.innerHTML = 'Please fill all fields'
                    alert.classList.add('alert')
                    document.querySelector('.modal-login__createvisitbtn').before(alert)
                }


            }


        })

    }
}