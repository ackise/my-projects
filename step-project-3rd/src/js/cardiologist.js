import {Visit} from './visit'
import {Card} from './card';



export class VisitCardiologist extends Visit {
    constructor() {
        super()
        
    }

    visitCardiologist() {
        super.renderForm();
        const cardioFields = document.createElement('div');
        cardioFields.classList.add('modal-login__doctor-fields');
        const bloodPresure = document.createElement('input')
        bloodPresure.setAttribute('placeholder', 'Usual blood presure')
        bloodPresure.classList.add('modal-login__login-input')
        const bodyIndex = document.createElement('input')
        bodyIndex.setAttribute('placeholder', 'Body index weight')
        bodyIndex.classList.add('modal-login__login-input')
        bodyIndex.setAttribute('type','number')
        const diseases = document.createElement('input')
        diseases.setAttribute('placeholder', 'Past diseases of the cardiovascular system')
        diseases.classList.add('modal-login__login-input')
        const age = document.createElement('input')
        age.setAttribute('placeholder','Your age' )
        age.classList.add('modal-login__login-input')
        age.setAttribute('type','number')
        cardioFields.append(bloodPresure, bodyIndex, diseases, age)

        document.querySelector('.modal-login__createvisitbtn').before(cardioFields)

        this.createVisitBtn.addEventListener('click', () => {
            
            let keyValues = {
                title: this.visitTitle.value,
                description: this.visitDescription.value,
                doctor: document.querySelector('.select-doctor').value,
                age: age.value,
                bloodPresure: bloodPresure.value,
                bodyIndex: bodyIndex.value,
                diseases: diseases.value,
                urgency: this.urgentList.value,
                fullname: this.visitFullName.value,
                visitreason: this.visitReason.value,
                status: 'open'
            }
            const formFields = Object.values(keyValues)
                const newArr = formFields.filter(e => e !== "");
                if (formFields.length === newArr.length) {
                    fetch('https://cards.danit.com.ua/cards', {
                        method: 'POST',
                        body: JSON.stringify(keyValues),
                        headers: {
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }
                    })
                        .then(res => res.json())
                        .then(res =>  new Card(res).render())
                        document.querySelector('.modal-login__closebtn').click()
                } else {
                    if(!document.querySelector('.alert')){
                        const alert = document.createElement('span')
                    alert.innerHTML = 'Please fill all fields'
                    alert.classList.add('alert')
                    document.querySelector('.modal-login__createvisitbtn').before(alert)
                    }
                    

                }

               
            }
        )

    }
}