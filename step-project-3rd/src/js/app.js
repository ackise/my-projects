import {Modal} from './modal'
import {Form} from "./form";


export class App {
    constructor(logo) {
        this.logoPath = logo;
        this.loginBtn = document.createElement('button');
        this.appHeader = document.createElement('div');
        this.container = document.querySelector('.app')
        this.logo = document.createElement('img')
        this.token = localStorage.getItem('token')
    }
    init() {
        sessionStorage.setItem("reloaded", true);
        this.render()
    }
    render() {
        this.logo.setAttribute('src', this.logoPath)
        this.logo.classList.add('header__logo')
        this.appHeader.classList.add('header')
        this.loginBtn.classList.add('btn')
        this.loginBtn.innerText = 'LOGIN'
        this.appHeader.append(this.logo, this.loginBtn)
        this.container.append(this.appHeader)
        if (sessionStorage.getItem("reloaded")) {
            localStorage.setItem('visit',true)
            localStorage.setItem('modal',true)
        }
        if(this.token) {
            let success = new Form().successReg()
        } else {
            this.loginBtn.addEventListener('click', () => {
                localStorage.setItem('visit',true)
                localStorage.setItem('modal',true)
                if(localStorage.getItem('modal') === 'true') {
                    const newModal = new Modal().init()
                    localStorage.setItem('modal',false)
                }
            })
        }
    }

}